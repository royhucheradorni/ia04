module gitlab.utc.fr/royhucheradorni/ia04.git

go 1.20

require (
	github.com/ankurjha7/jps v0.0.0-20201219090358-cbbbf3b632ea
	github.com/ebitenui/ebitenui v0.5.5
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0
	github.com/hajimehoshi/ebiten/v2 v2.6.1
	golang.org/x/exp v0.0.0-20231127185646-65229373498e
	golang.org/x/image v0.12.0
)

require (
	github.com/ebitengine/purego v0.5.0 // indirect
	github.com/go-faker/faker/v4 v4.2.0 // indirect
	github.com/jezek/xgb v1.1.0 // indirect
	golang.org/x/exp/shiny v0.0.0-20230817173708-d852ddb80c63 // indirect
	golang.org/x/mobile v0.0.0-20230922142353-e2f452493d57 // indirect
	golang.org/x/sync v0.3.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
	golang.org/x/text v0.14.0 // indirect
)
